package ictgradschool.industry.lab14.ex04;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Simple application to calculate the prime factors of a given number N.
 * 
 * The application allows the user to enter a value for N, and then calculates 
 * and displays the prime factors. This is a very simple Swing application that
 * performs all processing on the Event Dispatch thread.
 *
 * Some big numbers:
 * 999999999999989
 * 4444444444444463
 * 10000001400000049
 * 999999999999999989
 * 9201111169755555649
 */
public class PrimeFactorsSwingApp extends JPanel {

	private JButton _startBtn;        // Button to start the calculation process.
	private JButton _abortBtn;        // Button to abort the calculation process.
	private JTextArea _factorValues;  // Component to display the result.
	PrimeFactorisationWorker calcWorker;

	public PrimeFactorsSwingApp() {
		// Create the GUI components.
		JLabel lblN = new JLabel("Value N:");
		final JTextField tfN = new JTextField(20);
		
		_startBtn = new JButton("Compute");
		_abortBtn = new JButton("Abort");
		_factorValues = new JTextArea();
		_factorValues.setEditable(false);
		
		// Add an ActionListener to the start button. When clicked, the 
		// button's handler extracts the value for N entered by the user from 
		// the textfield and find N's prime factors.
		_startBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				String strN = tfN.getText().trim();
				long n = 0;

				try {
					n = Long.parseLong(strN);
				} catch (NumberFormatException e) {
					System.out.println(e);
				}

				// Disable the Start button until the result of the calculation is known.
				_startBtn.setEnabled(false);

				// Clear any text (prime factors) from the results area.
				_factorValues.setText(null);

				// Set the cursor to busy.
				setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

				// Start the SwingWorker running
				calcWorker = new PrimeFactorisationWorker(n);
				calcWorker.execute();
			}
		});

		_abortBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				// Re-enable the Start button.
				_startBtn.setEnabled(true);
				// Restore the cursor.
				setCursor(Cursor.getDefaultCursor());
				// Cancel SwingWorker
				calcWorker.cancel(true);
			}
		});
		
		// Construct the GUI. 
		JPanel controlPanel = new JPanel();
		controlPanel.add(lblN);
		controlPanel.add(tfN);
		controlPanel.add(_startBtn);
		controlPanel.add(_abortBtn);
		
		JScrollPane scrollPaneForOutput = new JScrollPane();
		scrollPaneForOutput.setViewportView(_factorValues);
		
		setLayout(new BorderLayout());
		add(controlPanel, BorderLayout.NORTH);
		add(scrollPaneForOutput, BorderLayout.CENTER);
		setPreferredSize(new Dimension(500,300));
	}

	private static void createAndShowGUI() {
		// Create and set up the window.
		JFrame frame = new JFrame("Prime Factorisation of N");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// Create and set up the content pane.
		JComponent newContentPane = new PrimeFactorsSwingApp();
		frame.add(newContentPane);

		// Display the window.
		frame.pack();
        frame.setLocationRelativeTo(null); 
		frame.setVisible(true);
	}

	private class PrimeFactorisationWorker extends SwingWorker<Void, Long> {

		long n;

		public PrimeFactorisationWorker(long n) {
			this.n = n;
		}

		@Override
		protected Void doInBackground() {
			// Start the computation in the Event Dispatch thread.

			for (long i = 2; i*i <= n && !isCancelled(); i++) {
				// If i is a factor of N, repeatedly divide it out
				while (n % i == 0) {
					publish(i);
					n = n / i;
				}
			}

			// if biggest factor occurs only once, n > 1
			if (n > 1 && !isCancelled()) {
				publish(n);
			}
			return null;
		}

		@Override
		protected void process(List<Long> chunks) {
			// Updates the messages text area
			for (Long factor : chunks) {
				_factorValues.append(factor + "\n");
			}
		}

		@Override
		public void done() {
			// Re-enable the Start button.
			_startBtn.setEnabled(true);
			// Restore the cursor.
			setCursor(Cursor.getDefaultCursor());
		}
	}


	public static void main(String[] args) {
		// Schedule a job for the event-dispatching thread:
		// creating and showing this application's GUI.
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}

