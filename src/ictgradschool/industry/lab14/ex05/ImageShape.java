package ictgradschool.industry.lab14.ex05;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.concurrent.ExecutionException;

/**
 * A shape which is capable of loading and rendering images from files / Uris / URLs / etc.
 */
public class ImageShape extends DynamicRectangleShape {

    private Image image;
    private URL url;

    public ImageShape(int x, int y, int deltaX, int deltaY, int width, int height, String fileName) throws MalformedURLException {
        this(x, y, deltaX, deltaY, width, height, new File(fileName).toURI());
    }

    public ImageShape(int x, int y, int deltaX, int deltaY, int width, int height, URI uri) throws MalformedURLException {
        this(x, y, deltaX, deltaY, width, height, uri.toURL());
    }

    public ImageShape(int x, int y, int deltaX, int deltaY, int width, int height, URL url) {
        super(x, y, deltaX, deltaY, width, height, Color.gray);

        this.url = url;
        this.execute();
    }


    @Override
    public void paint(Painter painter) {

        if (image != null) painter.drawImage(this.image, fX, fY, fWidth, fHeight);
        else {
            painter.setColor(Color.red);
            super.paint(painter);
            painter.drawCenteredText("Loading...", fX, fY, fWidth, fHeight);
        }

    }

    @Override
    protected Image doInBackground() throws Exception {
        Image image = ImageIO.read(url);
        if (!(fWidth == image.getWidth(null) && fHeight == image.getHeight(null))) {
            image = image.getScaledInstance(fWidth, fHeight, Image.SCALE_SMOOTH);
            image.getWidth(null);
        }

        return image;
    }

    @Override
    protected void done() {
        try {
            this.image = get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    // Include Swingworker as internal class to reduce overhead of having whole shape hierarchy dependent on swingworker.
}
