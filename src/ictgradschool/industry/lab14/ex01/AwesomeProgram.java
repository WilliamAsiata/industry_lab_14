package ictgradschool.industry.lab14.ex01;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.concurrent.ExecutionException;

/**
 * Created by wasi131 on 2/05/2017.
 */
public class AwesomeProgram implements ActionListener {

    private JLabel progressLabel /*= …*/;
    private JLabel myLabel /*= …*/;
    private JButton myButton /*= …*/;
    /** Called when the button is clicked. */

    @Override
    public void actionPerformed(ActionEvent e) {
        myButton.setEnabled(false);
// Start the SwingWorker running
        MySwingWorker worker = new MySwingWorker();
        worker.execute();
    }

    private class MySwingWorker extends SwingWorker<Integer, Void> {
        protected Integer doInBackground() throws Exception {
            int result = 0;
            for (int i = 0; i < 100; i++) {
// Do some long-running stuff
//                result += doStuffAndThings();
// Report intermediate results
                progressLabel.setText("Progress: " + i + "%");
            }
            return result;
        }

        @Override
        public void done() {
            // When the SwingWorker has finished, display the result in myLabel.
            myButton.setEnabled(true);
            int result;
            try {
                result = get();
                myLabel.setText("Result: " + result);
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            } catch (ExecutionException e1) {
                e1.printStackTrace();
            }
        }
    }
}
